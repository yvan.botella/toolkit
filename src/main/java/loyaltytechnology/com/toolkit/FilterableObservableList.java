package loyaltytechnology.com.toolkit;

import android.databinding.BaseObservable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import br.com.zbra.androidlinq.Linq;
import br.com.zbra.androidlinq.Stream;

/**
 * Created by yvan on 21/04/2016.
 */
public class FilterableObservableList<T> extends BaseObservable implements ObservableList<T> {

    //region Attributes
    private final ObservableList<T> _source = new ObservableArrayList<>();
    private final ObservableList<T> _current = new ObservableArrayList<>();
    private final List<FilterCallback<T>> filters = new ArrayList<>();
    //endregion

    //region Ctor
    public FilterableObservableList() {
        _source.addOnListChangedCallback(new OnListChangedCallback<ObservableList<T>>() {
            @Override
            public void onChanged(ObservableList<T> ts) {
               Log.e("OnListChangedCallback", "onChanged");
            }

            @Override
            public void onItemRangeChanged(ObservableList<T> ts, int i, int i1) {
                Log.e("OnListChangedCallback", String.format("onItemRangeChanged, %d, %d", i, i1));
            }

            @Override
            public void onItemRangeInserted(ObservableList<T> ts, int i, int i1) {
                Log.e("OnListChangedCallback", String.format("onItemRangeInserted, %d, %d", i, i1));
            }

            @Override
            public void onItemRangeMoved(ObservableList<T> ts, int i, int i1, int i2) {
                Log.e("OnListChangedCallback", String.format("onItemRangeMoved, %d, %d, %d", i, i1, i2));
            }

            @Override
            public void onItemRangeRemoved(ObservableList<T> ts, int i, int i1) {
                Log.e("OnListChangedCallback", String.format("onItemRangeRemoved, %d, %d", i, i1));
            }
        });
    }
    //endregion

    //region Filters
    public interface FilterCallback<T> {
        List<T> onFiltering(Stream<T> linq);
    }

    public boolean addFilter(FilterCallback<T> filter) {
        if (filter != null) {
            boolean state = filters.add(filter);
            filter();
            return state;
        }
        return true;
    }

    public boolean addFilters(Collection<FilterCallback<T>> filter) {
        if (filter != null) {
            boolean state = filters.addAll(filter);
            filter();
            return state;
        }
        return true;
    }

    public boolean removeFilter(FilterCallback<T> filter) {
        if (filter != null) {
            boolean state = filters.remove(filter);
            filter();
            return state;
        }
        return true;
    }

    public boolean removeFilters(Collection<FilterCallback<T>> filter) {
        if (filter != null) {
            boolean state = filters.remove(filter);
            filter();
            return state;
        }
        return true;
    }

    private void clearFilters() {
        filters.clear();
    }

    private boolean filter() {
        Iterator<FilterCallback<T>> i = filters.iterator();

        List result = new ArrayList(_source);

        try {
            while (i.hasNext()) {
                FilterCallback filter = i.next();
                if (filter != null) {
                    result.retainAll(filter.onFiltering(Linq.stream(result)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        _current.clear();
        _current.addAll(result);
        return true;
    }

    private boolean filterMatchItem(T item) {
        Iterator<FilterCallback<T>> i = filters.iterator();

        List result = new ArrayList();
        result.add(item);

        try {
            while (i.hasNext()) {
                FilterCallback filter = i.next();
                if (filter != null) {
                    result.retainAll(filter.onFiltering(Linq.stream(result)));
                    if (!result.contains(item)) {
                        return false;
                    }
                }
            }
            if (result.contains(item)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    public ObservableList<T> getSource() {
        return _source;
    }

    public ObservableList<T> getCurrent() {
        return _current;
    }
    //endregion

    //region SOURCE METHOD

    /**
     * addOnListChangedCallback from Source
     * @param onListChangedCallback
     */
    @Override
    public void addOnListChangedCallback(OnListChangedCallback<? extends ObservableList<T>> onListChangedCallback) {
        _current.addOnListChangedCallback(onListChangedCallback);
    }

    /**
     * removeOnListChangedCallback from Source
     * @param onListChangedCallback
     */
    @Override
    public void removeOnListChangedCallback(OnListChangedCallback<? extends ObservableList<T>> onListChangedCallback) {
        _current.removeOnListChangedCallback(onListChangedCallback);
    }

    /**
     * add to Source
     * @param location
     * @param object
     */
    @Override
    public void add(int location, T object) {
        _source.add(location, object);
        filter();
    }

    /**
     * add to Source
     * @param object
     * @return
     */
    @Override
    public boolean add(T object) {
        boolean state = _source.add(object);
        filter();
        return state;
    }

    /**
     * addAll to Source
     * @param location
     * @param collection
     * @return
     */
    @Override
    public boolean addAll(int location, Collection<? extends T> collection) {
        boolean state = _source.addAll(location, collection);
        filter();
        return state;
    }

    /**
     * addAll to Source
     * @param collection
     * @return
     */
    @Override
    public boolean addAll(Collection<? extends T> collection) {
        boolean state = _source.addAll(collection);
        filter();
        return state;
    }

    /**
     * clear Source
     */
    @Override
    public void clear() {
        _source.clear();
        filter();
    }

    /**
     * remove from Source
     * @param location
     * @return
     */
    @Override
    public T remove(int location) {
        T value = _source.remove(location);
        _current.remove(value);
        return value;
    }

    /**
     * remove from Source
     * @param object
     * @return
     */
    @Override
    public boolean remove(Object object) {
        boolean state = _source.remove(object);
        _current.remove(object);
        return state;
    }

    /**
     * removeAll from Source
     * @param collection
     * @return
     */
    @Override
    public boolean removeAll(Collection<?> collection) {
        boolean state = _source.removeAll(collection);
        _current.removeAll(collection);
        return state;
    }

    /**
     * contains from Source
     * @param object
     * @return
     */
    @Override
    public boolean contains(Object object) {
        return _source.contains(object);
    }

    /**
     * containsAll from Source
     * @param collection
     * @return
     */
    @Override
    public boolean containsAll(Collection<?> collection) {
        return _source.containsAll(collection);
    }

    /**
    * retainAll from Source
    * @param collection
    * @return
    */
    @Override
    public boolean retainAll(Collection<?> collection) {
        return collection.retainAll(collection);
    }
    //endregion

    //region CURRENT METHOD

    /**
     * equals to Current
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        return _source.equals(object);
    }

    /**
     * get from Current
     * @param location
     * @return
     */
    @Override
    public T get(int location) {
        return _current.get(location);
    }

    /**
     * hasCode of Current
     * @return
     */
    @Override
    public int hashCode() {
        return _current.hashCode();
    }

    /**
     * indexOf from Current
     * @param object
     * @return
     */
    @Override
    public int indexOf(Object object) {
        return _current.indexOf(object);
    }

    /**
     * isEmpty from Current
     * @return
     */
    @Override
    public boolean isEmpty() {
        return _current.isEmpty();
    }

    /**
     * iterator from Current
     * @return
     */
    @NonNull
    @Override
    public Iterator<T> iterator() {
        return _current.iterator();
    }

    /**
     * lastIndexOf from Current
     * @param object
     * @return
     */
    @Override
    public int lastIndexOf(Object object) {
        return _current.lastIndexOf(object);
    }

    /**
     *
     * @return
     */
    @Override
    public ListIterator<T> listIterator() {
        return _current.listIterator();
    }

    /**
     *
     * @param location
     * @return
     */
    @NonNull
    @Override
    public ListIterator<T> listIterator(int location) {
        return _current.listIterator(location);
    }

    /**
     * set to Current
     * @param location
     * @param object
     * @return
     */
    @Override
    public T set(int location, T object) {
        return null;
    }

    /**
     * size of Current
     * @return
     */
    @Override
    public int size() {
        return _current.size();
    }

    /**
     * subList from Current
     * @param start
     * @param end
     * @return
     */
    @NonNull
    @Override
    public List<T> subList(int start, int end) {
        return _current.subList(start, end);
    }

    /**
     * toArray from Current
     * @return
     */
    @NonNull
    @Override
    public Object[] toArray() {
        return _current.toArray();
    }

    /**
     * toArray from Current
     * @param array
     * @param <T1>
     * @return
     */
    @NonNull
    @Override
    public <T1> T1[] toArray(T1[] array) {
        return _current.toArray(array);
    }
    //endregion
}