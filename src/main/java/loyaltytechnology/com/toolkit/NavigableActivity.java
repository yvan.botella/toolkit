package loyaltytechnology.com.toolkit;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.util.Log;

import java.util.Stack;

/**
 * Created by yvan on 21/04/2016.
 */
public class NavigableActivity extends Activity {

    final Stack<Fragment> views = new Stack<>();
    private int idFragment;

    public NavigableActivity(int idFragment) {
        super();
        this.idFragment = idFragment;
    }

    public void navigateTo(Fragment view) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        ft.replace(idFragment, view);
        if (views.size() > 0) {
            ft.addToBackStack(null);
        }
        try {
            ft.commit();
        } catch (IllegalStateException e) {
            ft.commitAllowingStateLoss();
        }
        //ft.commit();

        views.push(view);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();

        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
            if (views.size() > 0) {
                views.pop();
            }
        } else {
            super.onBackPressed();
        }
    }
}
